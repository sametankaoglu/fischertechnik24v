# Fischertechnik_Factory_24V_S71500
This repo contains the source code for the cyclical sequence of a Fischertechnik-Fabrik-24v. The code has been expanded so that the factory sends its data to an edge device via MQTT in every cycle. In addition, the factory also provides the option of simulating maintenance after limit values have been exceeded. After the repair (factory stops for a specified time), the factory continues as usual. The functionality of the edge device was written in python which only sends the data from the factory to the cloud (Azure) and also receives data from the cloud. Since it can be quite complex to start this environment correctly, a detailed introduction on how to configure and start it follows. 

## Fischertechnik24V
The documentation on the fischertechnik24v factory can be looked up [here](https://www.fischertechnik.de/de-de/service/elearning/simulieren/fabrik-simulation-24v). To control the factory with all described functionality above, you need some hardware. In our case we use:
* (PLC) Siemens S7-1500 CPU 1511C-1 PN (x1)
* SIMATIC S7, Memory Card for S7-1x 00 CPU/SINAMICS, 3, 3V Flash, 4 Mbyte (x1)
* Profile rail (x1)
* PM 190W 120/230VAC (x1)
* DQ 32x24VDC/0.5A HF (x1)
* DI 32x24VDC HF (x1)
* SIMATIC S7-1500 Front connector Push-In-clamp für 35 mm-Modul (x2)
* Cabling

### Run the code for the factory
If the necessary hardware is available and all the cabling stuff is done, you need to run the code from this repo. For this an IDE is needed to build and run the code on the plc. We use TIA Portal V15.1. When the IDE is installed and the plc is connected successfully to your pc you can start to copy the [code](https://gitlab.com/sametankaoglu/fischertechnik24v/-/tree/main/Fischertechnik24V) and paste it into your new created project. To do this, follow these steps:

1. Look at the folder where the fischertechnik [code](https://gitlab.com/sametankaoglu/fischertechnik24v/-/tree/main/Fischertechnik24V) is, you can see six folders. In each folder you can find a Main.scl and a Startup.scl file. 
    - Copy the name of the folder and create in TIA Portal two scl-files for each folder. The first one with the "foldername" the second one "foldername_Startup".(exmpl. Oven, Oven_Startup). Important is that the "foldername_startup" file need to be initialized as startup file before it was created.
2. Now you can copy the code from each folder into the right scl-file in TIA-Portal.
3. After this you need to import the variables that you can find [here](https://gitlab.com/sametankaoglu/fischertechnik24v/-/blob/main/Variables.xlsx).
    - Download the xlsx file
    - Go in TIA Portal to PLC-variables and click on import. 
    - After this give the path to the xlsx file.
4. Configure the hsc-counters.
    - Activate HSC-counters from 1 to 5.
    - Signaltype: impulse (A) and direction (B)
    - Signal evaluation: Easy
    - To apply changes you need to compile offline. Go right click on plc and click on "load device-configuration changes" 
5. Configure timers and counters
    - Go over the code and open the regions where timers or counters can be found.
    - Right on the corner click on the arrow.
    - Now u can search after functions.
    - Search after TON for timer and CTU for counter
    - Grab and pull it into the code.
    - Give it exactly the same name like it can be found in the code.
    - Now you can delete the new one from the code.
6. To configure the mqtt-client just look at this [guide](https://cache.industry.siemens.com/dl/files/872/109748872/att_1006438/v4/109748872_MQTT_Client_DOKU_V2-1_en.pdf) and follow it from part 2.2.1 to 2.3.3.

When you do all these steps successfully you can now start the factory!

#### Need to know before start the factory
- When you go into the code you will see in the "Cooperation_all_stations_Startup scl" file that there are some limits declared. When these are not on "-1" it will do an maintenance when an limit is reached while the factory is running. Default is "-1". So you can add some limits if you wish a maintenance and you can add a maintenance duration. While maintenance it will blink the red light in the oven station.
- In most cases the hardware of the fischertechnik is very error-prone. Because it can always be that a stone can fall down you always have to be careful and correct. As soon as a stone gets stuck or falls, it is best to switch it off and pack all the stones into the warehouse.
- The start position: Always all stones are in the warehouse.
- To use the mqtt functionality go into "communication_with_edge_device_startup" and specify your ip adress 

## Edge-Device (Not important if you use the edge-device environment in the [shopfloor-edge-device](https://gitlab.com/sametankaoglu/shopfloor-digitization) repository) 
This is an edge device simulation written in python. This communicates with the factory and an azure cloud. We assume that an [iot hub](https://docs.microsoft.com/de-de/azure/iot-hub/iot-hub-create-through-portal) has already been created so that the edge device can communicate with it. 
1. Download the code in this [folder](https://gitlab.com/sametankaoglu/fischertechnik24v/-/blob/main/python-edge-device/python_mqtt_receiver_client.py) and install all specified packages. 
2. Before you start the python code, make sure that the [mosquitto mqtt broker](https://hub.docker.com/_/eclipse-mosquitto) is running and that port 1883 has been forwarded to the docker container. In this simulation Docker was used for containerization. So make sure that [docker](https://docs.docker.com/get-docker/) has been installed on the computer on which the python code is running. 
3. Now add your registered device [connection_strings](https://docs.microsoft.com/de-de/azure/iot-hub/quickstart-send-telemetry-python#register-a-device) to the code and save it. The variable where you need to add it, is the array "__iot_device_connection_strings". It must be a string, and if you have more than one connection string, separate each one in the "[]" with a comma.
4. After all these steps, the script should can be started and the factory can be started afterwards. 

If everything has been configured correctly, data will be sent from the factory to the edge-device and from there it will be cyclically sended to the iot hub. In our case the json that is sent to the cloud looks like this: ```{'oven': {'wear_saw': 1, 'lamp_wear': 1, 'need_maintance': 0},
'vacuumgripper': {'encoder_accumulation_of_rotation': 2702, 'need_maintance': 0},
'warehouse': {'import_white': 3, 'import_blue': 3, 'import_red': 3},
'sortingline': {'export_white': 1, 'export_blue': 0, 'export_red': 0},
'factory': {'days': 0, 'hours': 0, 'minutes': 3, 'seconds': 31}
}```
