from typing import Any
from azure.iot.device.iothub.models import message
import paho.mqtt.client as mqtt
import time
import json

# Using the Python Device SDK for IoT Hub:
# https://github.com/Azure/azure-iot-sdk-python
# The sample connects to a device-specific MQTT endpoint on your IoT Hub.
from azure.iot.device import IoTHubDeviceClient, Message

# -------------------------------- Variables -------------------------------- #

INTERVALL_SEND_DATA_TO_CLOUD_IN_SECONDS = 5

MQTT_CLIENT_ID                  = "Edge-Device"
MQTT_BROKER_ADDRESS             = "localhost"
MQTT_BROKER_PORT                = 1883
MQTT_SUBSCRIBED_TOPICS          = ["fischertechnik24v"] # Add to the array the topics u want to subscribe

# The device connection string to authenticate the device with your IoT hub.
# Using the Azure CLI:
# az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table
EDGE_DEVICE_CONNECTION_STRING                   = ""

# Json with all iot-device connection strings that are registered at the frontend
# Get filled by sending the keys from frontend to edge-device TODO 
__iot_device_connection_strings = []

# Json that will be sent to cloud and get updated by the s7-1500
__data_to_cloud = {
'oven':             {'wear_saw': 1, 'lamp_wear': 1, 'need_maintance': 0},
'vacuumgripper':    {'encoder_accumulation_of_rotation': 2702, 'need_maintance': 0},
'warehouse':        {'import_white': 3, 'import_blue': 3, 'import_red': 3}, 
'sortingline':      {'export_white': 1, 'export_blue': 0, 'export_red': 0}, 
'factory':          {'days': 0, 'hours': 0, 'minutes': 3, 'seconds': 31}
}

# -------------------------------- Functions -------------------------------- #

def iothub_client_init(connection_string):
    # Create an IoT Hub client
    print ( "Try to connect to iot-hub" )
    iothub_client = IoTHubDeviceClient.create_from_connection_string(connection_string)
    print ( "Connected to iot-hub" )

    return iothub_client

def iothub_client_send_data_to_iothub(data, connection_string):
    iot_hub_client = iothub_client_init(connection_string)

    try:
        # Convert json to string
        message = json.dumps(data)
    except:
        print ( "Cant dump json to string. Data cannot be sent to the iot hub" )
        return

    try:
        print ( "Try to send message to iot-hub" )
        iot_hub_client.send_message(message)
        print(str(message))
        print ( "Message successfully sent" )
    except:
        print( "Data cannot be sent to the iot hub. Exception raises at iot_hub_client.send_message()" )
    
    iot_hub_client.disconnect()

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

def on_message(client, userdata, msg):
    
    # Prints the topic and message.
    print(msg.topic + " " + str(msg.payload))

    try:
        # It will fail at this point when a number begins with an "+" 
        data = json.loads(msg.payload)
    except:
        # Replace all occupied "+" in the byte array. 
        data = msg.payload.replace(b'+',b'')
        data = json.loads(data)
        
    # Catch messages from topic fischertechnik and send it to iot-hub.
    # When an error occurs get data from iot-device again 
    if msg.topic == "fischertechnik24v":
        __data_to_cloud.update(data)
        client.publish("edge-device", "message_recieved")

def send_data_to_cloud(data, connection_strings):
    i = 0
    while i < len(connection_strings):
        iothub_client_send_data_to_iothub(data[connection_strings[i]], connection_strings[i+1])
        i += 2
                      
# Etablish a connection to the mqtt broker
client              = mqtt.Client(client_id = MQTT_CLIENT_ID)
client.on_connect   = on_connect
client.on_message   = on_message
client.connect(MQTT_BROKER_ADDRESS, port = MQTT_BROKER_PORT)

# Subscribe to all topics above given
for topic in MQTT_SUBSCRIBED_TOPICS:
    client.subscribe(topic + "/#")

client.loop_start()

# -------------------------------- Main Loop -------------------------------- #
while True:
    print("Edge-device is running")
    time.sleep(INTERVALL_SEND_DATA_TO_CLOUD_IN_SECONDS)
    send_data_to_cloud(__data_to_cloud, __iot_device_connection_strings)

